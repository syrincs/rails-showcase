FactoryGirl.define do
  factory :unconfirmed_transporter, :class => Transporter do
    sequence(:company_name) {|n| "Your Taxi's #{n}"}
    operational_manager 'John Doe'
    sequence(:email) { |n| "johndoe#{n}@yourtaxi.com" }
    password 'hehtaxi'
    password_confirmation 'hehtaxi'
    
    factory :confirmed_transporter do
      confirmed_at { Date.yesterday }
      after_build { |t| raise "Transporter is unexpectedly confirmed" if not t.confirmed? }
    end
  end
  
  
  factory :transporter, :parent => :confirmed_transporter do
    sequence(:general_manager) {|n| "Jan de Vries #{n}" }
    general_email "info@yourtaxi.com"
    phone_number '020 1234567'
    website 'http://www.yourtaxi.com'
    country_code 'NL'
    street 'Oostelijke Handelskade'
    house_number '35A'
    city 'Amsterdam'
    zip ''
    kvk '12345678'
    vat_number 'NL123456789B01'
    office_hours_start '09:00'
    office_hours_end '17:00'
    time_zone 'Amsterdam'
    booking_phone '020-2468013'
    sequence(:booking_email) { |n| "taxiboeken#{n}@yourtaxy.com" }
    bank_account_holder 'Jan de Vries'
    bank_account_number '1234567890'
    bic_swift_number '1234567890'
    iban_number '1234567890'
    bank_account_country_code "NL"
    currency 'EUR'
    works_on ["0", "1", "2", "3", "4", "5", "6"]
    office_open_on ["0", "1", "2", "3", "4", "5", "6"]
    operational_hours_start '07:00'
    operational_hours_end '23:00'
    minimum_time_for_changes_before_pickup '00:30'
    distance_metric 'kilometers'
    airports {[FactoryGirl.build(:airport)]}
    after_build { |transporter| raise "Transporter is unexpectedly invalid #{transporter.errors.inspect}" if not transporter.valid? }
    ratings {[FactoryGirl.build(:rating)]}

    factory :zilverline_transporter do
      company_name "Zilverline"
    end

    factory :swiss_transporter do
      company_name "Swiss Taxi"
      airports {[FactoryGirl.build(:airport_zurich)]}
    end
    
    factory :accepted_transporter do
      general_conditions_accepted true
    end
    
    factory :approved_transporter do
      general_conditions_accepted true
      approved true
    end
  end
end
