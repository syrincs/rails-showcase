require 'spec_helper'

describe "Transporter" do
  include TransporterHelper
  
  before :each do
    Geocoder.stubs(:search => [stub(:country_code => "NL", :latitude => 52, :longitude => 4)])
    Geonames::WebService.stubs(:timezone => stub(:gmt_offset => 1))
  end

  describe "notifications for changes" do
    it "should send changes to YAT when an approved transporter changes their profile" do
      zilverline = FactoryGirl.create(:approved_transporter)
      sign_in_transporter zilverline

      click_on "edit_transporter_company"
      fill_in "transporter_company_name", :with => "New name"
      fill_in "transporter_general_manager", :with => "John Doe"
      click_on "update_transporter"
      
      ActionMailer::Base.deliveries.size.should == 1
      mail = ActionMailer::Base.deliveries.first
      mail.to.first.should == EMAIL_YAT_ADMIN
      mail.encoded.should have_content I18n.t("watch_changes_notifier.notify.info_line", :field => I18n.t("activerecord.attributes.transporter.company_name"), :from => zilverline.company_name, :to => "New name")
      mail.encoded.should have_content I18n.t("watch_changes_notifier.notify.info_line", :field => I18n.t("activerecord.attributes.transporter.general_manager"), :from => zilverline.general_manager, :to => "John Doe")
    end
    
    it "should not send changes to YAT when a transporter changes their profile but is not yet approved" do
      ActionMailer::Base.deliveries.clear
      
      zilverline = FactoryGirl.create(:zilverline_transporter)
      zilverline.should_not be_approved
      sign_in_transporter zilverline

      click_on "edit_transporter_company"
      fill_in "transporter_company_name", :with => "New name"
      fill_in "transporter_general_manager", :with => "John Doe"
      click_on "update_transporter"
      
      ActionMailer::Base.deliveries.should be_empty
    end
  end

  describe "record owner" do
    before :each do
      @transporter = FactoryGirl.create(:confirmed_transporter)
      sign_in_transporter @transporter
    end

    it "should render account part" do
      page.should have_css "#transporter-account-details"
    end

    describe "edit" do
      describe "account" do
        before :each do
          click_on "edit_transporter_account"
        end

        it "should include password fields" do
          page.should have_css "#transporter_current_password"
          page.should have_css "#transporter_password"
          page.should have_css "#transporter_password_confirmation"
        end

        it "allow changing operational manager" do
          fill_in 'transporter_operational_manager', :with => 'Pietje Puk'
          fill_in 'transporter_current_password', :with => 'mumheyfx'

          click_on 'update_transporter'

          @transporter.reload
          @transporter.operational_manager.should == 'Pietje Puk'
        end

        it "allow changing password" do
          password_before = @transporter.encrypted_password
          fill_in 'transporter_current_password', :with => 'mumheyfx'
          fill_in 'transporter_password', :with => 'foobar'
          fill_in 'transporter_password_confirmation', :with => 'foobar'
          click_on 'update_transporter'

          @transporter.reload
          @transporter.encrypted_password.should_not == password_before
        end

        it "not change password when current not correct" do
          password_before = @transporter.encrypted_password
          fill_in 'transporter_current_password', :with => 'flarp!'
          fill_in 'transporter_password', :with => 'foobar'
          fill_in 'transporter_password_confirmation', :with => 'foobar'
          click_on 'update_transporter'
          page.should have_css ".field_with_errors #transporter_current_password"

          @transporter.reload
          @transporter.encrypted_password.should == password_before
        end
      end
    end
  end
end
