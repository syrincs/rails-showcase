module TransportersHelper
  def transporter_parts
    TRANSPORTER_PARTS
  end

  def transporter_current?(part)
    current_part == part
  end

  def transporter_path_for(part)
    if part == "rating"
      transporter_ratings_path
    else
      transporter_edit_part_registration_path(:part => part)
    end
  end

  def admin_transporter_path_for(part)
    if part == "rating"
      admin_transporter_ratings_path(@transporter)
    else
      edit_part_admin_transporter_path(@transporter, :part => part)
    end
  end

  def transporter_airports(transporter)
    content_tag :ul do
      transporter.airports.sort_by(&:code).each do |airport|
        concat(content_tag(:li, airport.code))
      end
    end
  end

  def transporter_categories(transporter)
    content_tag :ul do
      transporter.ratings.categories_with_total_count.sort_by(&:category).each do |category|
        concat(content_tag(:li, "#{t("activerecord.values.rating.category.#{category.category}")} (#{category.total_count.to_i})"))
      end
    end
  end
end
