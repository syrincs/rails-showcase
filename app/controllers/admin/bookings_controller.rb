class Admin::BookingsController < Admin::BaseController
  
  def index
    @bookings = Booking.contains_text(params[:search])
  end
  
  def show
    @booking = Booking.find(params[:id])
  end
  
end