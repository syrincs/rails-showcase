require 'digest/sha1'
require 'uri'

class BookingsController < ApplicationController

  before_filter :find_booking, :only => [:accept, :decline, :payment, :thanks]
  before_filter :validate_token_and_sign_in_transporter, :only => [:accept, :decline]
  before_filter :session_booking, :only => [:search, :offers, :confirm, :create]

  def new
    @booking = Booking.new
    session[:booking] = {}
  end

  def search
    if @booking.valid_route?
      booking = @booking.dup
      @offers = Hash[Rating::CATEGORIES.map { |category| [category, Transporter.offers_for_category(booking.tap { |b| b.category = category })] }.select { |_, offers| offers.present? }]
    else
      render :new
    end
  end

  def offers
    @ratings = Transporter.offer(@booking)
  end

  def confirm
  end

  def create
    @booking.update_location_information if @booking.valid?

    if @booking.save
      redirect_to payment_booking_path(@booking)
    else
      render :confirm
    end
  end

  def payment
    @options = {
        :pspid => PAYMENT_PROVIDER_PSPID,
        :orderid => "#{Time.now.strftime("%Y%m%d")}_#{@booking.id}",
        :amount => @booking.price_in_cents,
        :currency => "EUR", # TODO localize
        :language => "en", # TODO localize
        :title => I18n.t("bookings.payment.title"),
        :accepturl => thanks_booking_url(@booking),
        :homeurl => root_url,
        :cn => @booking.name,
        :email => @booking.email
    }
    @options[:shasign] = Ogone.signature_for_payment(@options, PAYMENT_PROVIDER_REQUEST_SIGNATURE)
  end

  def thanks
    raise "payment has been tampered with" unless Ogone.verify_signature(params)

    if Ogone.successful?(params)
      @booking.update_attributes!(:payment_id => params['PAYID'],
                                  :payment_card_number => params['CARDNO'],
                                  :payment_status => params['STATUS'],
                                  :payment_brand => params['BRAND'],
                                  :payment_authorized_at => Time.now)
      @booking.paid!
    elsif Ogone.cancelled?(params)
      render :payment_cancelled
    elsif Ogone.refused?(params)
      render :payment_refused
    else
      raise "unexpected response from payment provider"
    end
  end

  def accept
    if not @booking.accepted_by_transporter!
      flash[:alert] = I18n.t("activerecord.errors.models.booking.cant_accept_declined", :booking => @booking)
    end
    redirect_to transporter_bookings_url
  end

  def decline
    if not @booking.declined_by_transporter!
      flash[:alert] = I18n.t("activerecord.errors.models.booking.cant_decline_accepted", :booking => @booking)
    end
    redirect_to transporter_bookings_url
  end

  private

  def find_booking
    @booking = Booking.find(params[:id])
  end

  def validate_token_and_sign_in_transporter
    if @booking.token == params[:token]
      sign_in(@booking.transporter, :bypass => true)
    else
      Rails.logger.warn("access denied for: #{request.inspect}")
      redirect_to root_url
    end
  end

  def session_booking
    attrs = (session[:booking] || {}).merge(params.key?(:booking) ? params[:booking] : params) rescue {}
    @booking = Booking.new(attrs)
    session[:booking] = @booking.attributes
  end
end
