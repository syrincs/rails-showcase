class RouteCalculatorController < ApplicationController
  include ActionView::Helpers::NumberHelper
  
  def calculate_price
    category = params[:category]
    airport = Airport.find(params[:airport_id])
    distance_meters = Google::Maps.route("#{airport.lat_long}", params[:address]).distance.value
    
    unit_system = params[:unit_system]
    minimum_cost = BigDecimal.new(Numeric.parse_localized(params[:minimum_cost]))
    rates = params[:rates].values.map{|attrs| Rate.new(attrs)}.select(&:valid?)
    
    price = calculate(rates, minimum_cost, distance_meters, unit_system)
    prices = Rating.with_approved_transporter.with_category(category).for_airport(airport).map do |rating|
      calculate(rating.rates, rating.minimum_cost, distance_meters, rating.transporter.distance_metric)
    end

    render :json => {
      :price => number_with_precision(price, :precision => 2),
      :minimum => number_with_precision(prices.min, :precision => 2),
      :maximum => number_with_precision(prices.max, :precision => 2),
      :distance => distance_for_unit_system_text(distance_meters, params[:unit_system]),
      :result => "success"
    }
  rescue Google::Maps::InvalidResponseException => ex
    render :json => {:result => "error", :message => t("route.no_possible_route")}
  end
  
private
  
  def calculate(rates, minimum_cost, distance_meters, unit_system)
    RouteCostCalculator.calculate_price(minimum_cost,
                                        rates,
                                        BigDecimal.new((unit_system == UNIT_SYSTEM_KILOMETERS ?
                                                        distance_meters / 1000.0 :
                                                        Geocoder::Calculations.to_miles(distance_meters / 1000.0)).to_s))
  end
  
  def distance_for_unit_system_text(distance_meters, unit_system = UNIT_SYSTEM_KILOMETERS)
    distance_km = distance_meters / 1000.0
    if unit_system == UNIT_SYSTEM_KILOMETERS
      t("route_calculator.distance.km",
        :distance => number_with_precision(distance_km.round(2),
                                           :precision => 2))
    else
      t("route_calculator.distance.mi",
        :distance => number_with_precision(Geocoder::Calculations.to_miles(distance_km).round(2),
                                           :precision => 2))
    end
  end
end
