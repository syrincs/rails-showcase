require "dummy_view"

class Booking < ActiveRecord::Base
  belongs_to :rating
  belongs_to :airport

  default_scope order("bookings.created_at DESC")

  delegate :transporter, :to => :rating

  attr_accessible :pickup_time, :name, :address, :airport_id, :rating_id, :category, :direction, :email, :telephone, :payment_id, :payment_card_number, :payment_status, :payment_brand, :passenger_count, :airport, :price

  validates_presence_of :pickup_time, :name, :email, :telephone, :distance, :address, :direction, :airport, :rating
  validates_numericality_of :passenger_count, :greater_than => 0
  
  validates_format_of :email, :with => EMAIL_RE
  validates_format_of :telephone, :with => TELEPHONE_RE

  validate do |booking|
    booking.errors.add(:address, I18n.t("transfer.not_found")) unless booking.airport.present? && booking.address.present? && booking.distance.present?
  end

  validate do |booking|
    if booking.pickup_time < Time.now + MINIMAL_RESERVATION_TIME_IN_ADVANCE
      booking.errors.add(:pickup_time, I18n.t("transfer.can_not_book_so_close_in_advance",
                                              :minimum_time => DummyView.in_words(MINIMAL_RESERVATION_TIME_IN_ADVANCE)))
    end
  end

  validate do |booking|
    if booking.rating.present? && !booking.rating.transporter.airports.include?(booking.airport)
      booking.errors[:base] << I18n.t("transfer.invalid_rating")
    end
  end

  before_validation :prepare_price_distance

  status_field :status, :with => %w(new paid accepted_by_transporter declined_by_transporter)

  scope :contains_text, lambda { |query|
    query.present? and where("bookings.email LIKE :q OR bookings.name LIKE :q OR bookings.id LIKE :q", :q => "%#{query}%")
  }
  scope :without_new, where("bookings.status <> 'new'")

  before_create do |booking|
    booking.token ||= SecureRandom.hex(11)
  end

  after_save do |booking|
    if booking.status_changed? && booking.paid?
      BookingNotifier.confirmation(booking).deliver
      BookingNotifier.notification(booking).deliver
    end
  end

  def valid_route?
    valid? || [:address, :direction, :airport_id, :pickup_time, :passenger_count].all? { |k| errors[k].blank? }
  end

  def to_s
    "#{id}"
  end

  def price_in_cents
    (price * 100).to_i
  end

  def accepted_by_transporter!
    if self.accepted_or_declined_at.present?
      false
    else
      self.status = "accepted_by_transporter"
      self.accepted_or_declined_at = Time.now
      save!
    end
  end

  def declined_by_transporter!
    if self.accepted_or_declined_at.present?
      false
    else
      self.status = "declined_by_transporter"
      self.accepted_or_declined_at = Time.now
      save!
    end
  end
  
  def from_airport?
    self.direction == DIRECTION_FROM_AIRPORT
  end

  def to_airport?
    self.direction == DIRECTION_TO_AIRPORT
  end

  def pickup(latlong=false)
    self.from_airport? ? (latlong ? self.airport.latlong : self.airport) : self.address
  end

  def dropoff(latlong=false)
    self.to_airport? ? (latlong ? self.airport.latlong : self.airport) : self.address
  end

  def distance
    self[:distance] || route_distance
  end

  def price
    self[:price] || rating.try(:price, self)
  end

  def category
    self[:category] || rating.try(:category)
  end

  def vehicles_needed
    rating.vehicles_needed(passenger_count)
  end
  
  def pickup_latlong
    "#{pickup_latitude}, #{pickup_longitude}" if pickup_latitude && pickup_longitude
  end

  def dropoff_latlong
    "#{dropoff_latitude}, #{dropoff_longitude}" if dropoff_latitude && dropoff_longitude
  end

  def update_location_information
    self.pickup_address = route.start_address
    self.dropoff_address = route.end_address
    self.pickup_latitude = route.start_location.lat
    self.pickup_longitude = route.start_location.lng
    self.dropoff_latitude = route.end_location.lat
    self.dropoff_longitude = route.end_location.lng
  end

private
  
  def route
    @route ||= Google::Maps::Route.new(pickup(true), dropoff(true), I18n.locale.to_sym)
  end

  def route_distance
    # TODO let the google maps gem handle this conversion
    value = route.distance.value
    value && value / 1000.0
  rescue Google::Maps::InvalidResponseException => error
    Rails.logger.warn "Google Maps exception: #{error}"
    nil
  end

  def prepare_price_distance
    self.distance = route_distance unless self[:distance]
    self.price = rating.try(:price, self) if self[:price].blank? && distance.present?
  end
end
