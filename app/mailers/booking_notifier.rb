class BookingNotifier < ActionMailer::Base
  helper :application
  
  default from: EMAIL_NOREPLY

  def confirmation(booking)
    @booking_args = booking_args(booking)
    mail to: booking.email
  end

  def notification(booking)
    @booking_args = booking_args(booking).merge(:accept_url => accept_booking_url(booking, booking.token),
                                                :decline_url => decline_booking_url(booking, booking.token))
    mail to: booking.transporter.booking_email
  end
  
private
  
  def booking_args(booking)
    booking.attributes.
      merge(booking.transporter.attributes.map{|k,v| {"transporter_#{k}" => v}}.reduce(&:merge)).
      merge(:airport => booking.airport,
            :pickup_time_s => I18n.l(booking.pickup_time, :format =>:long_with_day)).symbolize_keys
  end
end
